package co.ferdroid.meflipcard;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import Entidad.Player;


public class MainActivity extends AppCompatActivity {

    MainGame game;

    ArrayList<ImageView> images_card;
    ArrayList<Player> players;

    private LinearLayout linear_images_cards;
    private LinearLayout linear_players_grid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle extras = getIntent().getExtras();


        if (extras != null) {

            String[] array_players = extras.getStringArray("players_name");
            int player_turn = extras.getInt("turn");

            /*
             * It needs instantiate textview located in the respective
             * linear layouts for the player's grid
             */
            linear_players_grid = (LinearLayout) findViewById(R.id.players_grid);

            players = new ArrayList<Player>();

            for (int i = 0; i < linear_players_grid.getChildCount(); i++) {
                LinearLayout linear_child = (LinearLayout) linear_players_grid.getChildAt(i);

                TextView player_name = (TextView) linear_child.getChildAt(0);
                TextView player_score = (TextView) linear_child.getChildAt(1);

                player_name.setText( array_players[i] );

                players.add(i, new Player( this, player_name, player_score ));

                //Log.d("@ players grid ", players.get(i).getName());
            }

            // Player selected starts flipping first card
            players.get(player_turn).setTurn(true);



            /*
             * Instantiate card's grid
             */
            linear_images_cards = (LinearLayout) findViewById(R.id.linear_images_cards);
            images_card = new ArrayList<ImageView>();

            int count_card = 0;
            for (int i = 0; i < linear_images_cards.getChildCount(); i++) {
                LinearLayout linear_child = (LinearLayout) linear_images_cards.getChildAt(i);
                for (int j = 0; j < linear_child.getChildCount(); j++) {
                    ImageView images_child = (ImageView) linear_child.getChildAt(j);
                    images_card.add(count_card, (ImageView) images_child.findViewById(images_child.getId()) );

                    //Log.d("@ images_card ", images_card.get(j).getTag().toString());
                    count_card++;
                }
            }


            game = new MainGame(this, players);
            game.create_board();


        }


    }





    public void preparar(View view) {

        String[] tag_array = view.getTag().toString().split("~");
        int i = Integer.parseInt(tag_array[2]);

        game.start(images_card.get(i-1), tag_array);

    }


    @Override
    public void onBackPressed() {

        Intent entrada_jugadores = new Intent(this, EntradaNombreJugador.class);
        startActivity(entrada_jugadores);

        finish();
    }



}
