package co.ferdroid.meflipcard;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import Entidad.BoardGame;
import Entidad.Player;
import Helpers.GameInterface;
import Helpers.Util;

/**
 * Created by Andres Fernando M. on 30/11/16.
 */

public class MainGame implements GameInterface {

    private Context context;

    private int turn = 0;

    private int add_if_matches = 2;
    private int substract_if_differents = 2;

    private BoardGame boardGame;
    private ArrayList<Player> players;

    private boolean tie = false; // determine if the game was tied

    private int matches = 0; // total pairs found

    private ImageView previous_img;

    private TextView notifications;
    private LinearLayout gameover_buttons;


    private ProgressDialog progress;

    private Util util;


    public MainGame(Context context, ArrayList<Player> players) {
        this.context = context;
        this.util = new Util(context);
        this.util.setDialogButtons(context.getString(R.string.alert_okbtn), context.getString(R.string.alert_cancelbtn));

        this.players = players;

        notifications = (TextView) ((Activity) context).findViewById(R.id.notifications_inapp);
        gameover_buttons = (LinearLayout) ((Activity) context).findViewById(R.id.gameover_buttons);

        progress = util.showProgress(context, context.getString(R.string.msg_hiding_cards));

    }


    @Override
    public void create_board() {
        boardGame = new BoardGame(context);
        boardGame.assign_cards();

        check_turn(true);

        notifications.setText(context.getString(R.string.msg_start_playerturn) + " " + (players.get(turn).getName()));

        players.get(turn).getTxt_name().setTextColor(ContextCompat.getColor(context, R.color.colorPlayerTurn));


        players.get(turn).countdownTimer();
    }



    @Override
    public void start(ImageView img, String[] tags) {

        int[][] tokens = boardGame.get_tokens();
        String[] cards = boardGame.getCards();

        int rows = Integer.parseInt(tags[0]); //
        int columns = Integer.parseInt(tags[1]);

        String selected_card = players.get(turn).play(img, rows, columns, tokens, cards);
        if (selected_card != null) {
            //Log.d("@@ selected card ", selected_card);
            compare_cards(img, selected_card);
        }

    }



    @Override
    public void compare_cards(final ImageView img_card, String card) {

        players.get(turn).setScore(0);

        if (boardGame.getFlipped_card().equals("")) {
            this.previous_img = img_card;
            boardGame.setFlipped_card(card);
        } else {
            boardGame.setCurrent_card(card);

            String flipped = boardGame.getFlipped_card();
            String current = boardGame.getCurrent_card();

            String[] p_score = players.get(turn).getTxt_score().getText().toString().split(":");

            int score = Integer.parseInt(p_score[1].trim());
            players.get(turn).setScore(score);

            if (flipped.equals(current)) {
                players.get(turn).setScore(score + add_if_matches);
                players.get(turn).setPairs( players.get(turn).getPairs() + 1 );

                this.matches += 1;

                this.previous_img.setOnClickListener(null); // disable listener for both players
                img_card.setOnClickListener(null); // for both players
            } else {

                this.progress.show();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        ((Activity)context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {


                                previous_img.setImageResource(MainGame.this.context.getResources().getIdentifier("card_placeholder", "drawable", MainGame.this.context.getPackageName()));
                                img_card.setImageResource(MainGame.this.context.getResources().getIdentifier("card_placeholder", "drawable", MainGame.this.context.getPackageName()));

                                img_card.setContentDescription("0");
                                previous_img.setContentDescription("0");

                                progress.dismiss();


                            }
                        });


                    }
                }, 500);

                if (score > 0) {
                    players.get(turn).setScore(score - substract_if_differents);
                }

            }

            boardGame.setFlipped_card("");
            boardGame.setCurrent_card("");

            check_turn(false);

        }


    }



    @Override
    public void check_turn(boolean assign) {
        if (assign) { // set the player's turn for the first time
            if (players.get(turn).isTurn()) {
                this.turn = 0;
            } else {
                this.turn = 1;
            }
        } else { // changing player's turn
            players.get(turn).getTxt_score().setText( context.getString(R.string.lbl_score) + " " + Integer.toString( players.get(turn).getScore() ) );

            players.get(turn).setTurn(false); // currently player turn is now false
            if (this.turn == 0) { // but what does turn is? is zero?
                players.get(turn).getTxt_name().setTextColor(ContextCompat.getColor(context, R.color.colorPlayerNotTurn));
                this.turn = 1; // well, the new turn going to be one
            } else {
                players.get(turn).getTxt_name().setTextColor(ContextCompat.getColor(context, R.color.colorPlayerNotTurn));
                this.turn = 0; // else new turn going to be zero
            }
            players.get(turn).setTurn(true); // set the player turn to the new one
            players.get(turn).getTxt_name().setTextColor(ContextCompat.getColor(context, R.color.colorPlayerTurn));


            // Check if all cards are flipped in order to finish game
            if (matches == Math.floor(boardGame.getCards().length / 2)) {
                finish_game();
            }


            notifications.setText(context.getString(R.string.msg_turn) + " " + players.get(turn).getName());

        }
    }



    @Override
    public void finish_game() {
        //Log.d("@@ finish ", "finishing the game now!");

        int sw=0; // winner's score
        String winner = ""; // winner's name
        boolean tie = false;

        String gameover_message = "";


        /*
         * To determine who does win the match
         * or if there is a tie
         */
        for (int i = 0; i < players.size(); i++) {
            if (players.get(i).getScore() > sw) {
                sw = players.get(i).getScore();
                winner = players.get(i).getName();
            } else if (players.get(i).getScore() == sw) {
                tie = true;
            }
        }

        if (tie) {
            gameover_message = context.getString(R.string.msg_tie);
        } else {
            gameover_message = winner + " " + context.getString(R.string.msg_player_win);
        }

        util.showAlert(context.getString(R.string.app_name), gameover_message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                notifications.setText(context.getString(R.string.msg_finished_game));
                gameover_buttons.setVisibility(View.VISIBLE);

            }
        }, null, 0);


    }



}
