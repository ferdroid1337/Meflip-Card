package Helpers;

import android.view.View;
import android.widget.ImageView;

/**
 * Created by Andres Fernando M. on 16/12/16.
 */

public interface GameInterface {


    /**
     * Instanciate board and players here
     */
    void create_board();


    /**
     * Start the Game Here
     * checking first player's turn
     * @param img
     * @param tags
     */
    void start(ImageView img, String[] tags);

    /**
     * @param img_card
     * @param card
     */
    void compare_cards(ImageView img_card, String card);


    /**
     * Checking what does player keep playing
     * or assign in case where it is a new game
     * @param assign
     */
    void check_turn(boolean assign);

    /**
     * Finish and count matched pairs founds
     * and scores
     */
    void finish_game();

}
