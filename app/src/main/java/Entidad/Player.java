package Entidad;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import co.ferdroid.meflipcard.EntradaNombreJugador;
import co.ferdroid.meflipcard.R;


/**
 * Created by Andres Fernando M. on 28/10/16.
 */

public class Player {

    private String name;

    private int score = 0;

    private boolean turn = false;

    private String[] gametime;

    private Context context;

    private TextView txt_name;
    private TextView txt_score;

    private int pairs = 0; // pairs that player has found


    /**
     * @param ctx
     * @param textview_name
     * @param textview_score
     */
    public Player(Context ctx, TextView textview_name, TextView textview_score) {

        this.context = ctx;

        this.txt_name = textview_name;
        this.txt_score = textview_score;

        String time = "00:01:00"; // player has one minute left
                                  // and the time pauses when it isnt his/her turn
        Log.d("@ init time ", time);

        this.gametime = time.split(":");
        Log.d("@ gametime ", gametime[0] + " " + gametime[1] + " " + gametime[2]);

        this.name = textview_name.getText().toString();
        this.score = Integer.parseInt(textview_score.getText().toString());

        textview_score.setText( ctx.getString(R.string.lbl_score) + " " + this.score );

    }


    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isTurn() {
        return turn;
    }

    public void setTurn(boolean turn) {
        this.turn = turn;
    }



    public String play(ImageView img, int row, int col, int[][] tokens, String[] cards) {

        String h = img.getContentDescription().toString();

        if (h.equals("0")) {
            img.setContentDescription("1");
            String a = cards[ tokens[row][col] - 1 ];

            img.setImageResource(context.getResources().getIdentifier(a.toString(), "drawable", context.getPackageName()));

            try {
                AssetFileDescriptor descriptor = context.getAssets().openFd("sounds/" + a.toString() + ".mp3");

                MediaPlayer mp = new MediaPlayer();
                long aud_start = descriptor.getStartOffset();
                long aud_end = descriptor.getLength();
                mp.setDataSource(descriptor.getFileDescriptor(), aud_start, aud_end);
                mp.prepare();
                mp.setVolume(1.0f, 1.0f);
                mp.start();

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

            return a;

        } else {
            return null;
        }


    }


    public int getPairs() {
        return pairs;
    }

    public void setPairs(int pairs) {
        this.pairs = pairs;
    }

    public TextView getTxt_name() {
        return txt_name;
    }

    public TextView getTxt_score() {
        return txt_score;
    }




    public void countdownTimer() {

        int hours = Integer.parseInt(gametime[0]);
        int minutes = Integer.parseInt(gametime[1]);
        int seconds = Integer.parseInt(gametime[2]);


        do {

            if (seconds <= 0) {
                minutes--;
                Log.d("@ minutes ", Integer.toString(minutes));
                if (minutes <= 10) {
                    if (minutes < 0) {
                        gametime[1] = "59";
                        minutes = 59;
                    } else {
                        gametime[1] = "0" + (minutes);
                    }
                } else {
                    gametime[1] = Integer.toString(minutes);
                }
            }

            if (minutes <= 0) {
                hours--;
                Log.d("@ hours ", Integer.toString(hours));
                if (hours <= 10) {
                    if (hours < 0) {
                        gametime[0] = "01";
                        hours = 1;
                    } else {
                        gametime[0] = "0" + (hours);
                    }
                } else {
                    gametime[0] = Integer.toString(hours);
                }
            }

            seconds--;
            Log.d("@ seconds ", Integer.toString(seconds));
            if (seconds <= 10) {
                if (seconds < 0) {
                    gametime[2] = "59";
                    seconds = 59;
                } else {
                    gametime[2] = "0" + seconds;
                }
            } else {
                gametime[2] = Integer.toString(seconds);
            }

            Log.d("@ time player " + this.getName(), gametime[0] + ":" + gametime[1] + ":" + gametime[2]);

        } while (hours > 0 && minutes > 0 && seconds > 0);



    }


    public String[] getGametime() {
        return gametime;
    }



}
