package Entidad;

import android.content.Context;
import android.widget.Toast;

import co.ferdroid.meflipcard.R;

import static Helpers.Constantes.*;


/**
 * Created by Andres Fernando M. on 28/10/16.
 */

public class BoardGame {


    private int[][] _tokens;
    private String[] cards;


    private String flipped_card = "";
    private String current_card = "";


    private Context context;



    public BoardGame(Context ctx) {
        this._tokens = new int[BOARD_LENGTH / BOARD_DIMENSION][BOARD_LENGTH / BOARD_DIMENSION];

        cards = new String[]
                            { "ic_i001", "ic_i002", "ic_i003", "ic_i004",
                              "ic_i005", "ic_i006", "ic_i007", "ic_i008",
                              "ic_i001", "ic_i002", "ic_i003", "ic_i004",
                              "ic_i005", "ic_i006", "ic_i007", "ic_i008"
                            };

        // en el array de cartas se ponen las 8 imagenes repetidas
        /*
         * | m  p  l a |
         * | md ml s f |
         * | m  p  l a |
         * | md ml s f |
        */

        this.context = ctx;
    }


    public void assign_cards() {
        int i = 0;
        while (i < BOARD_LENGTH) {
            // Math.random() nunca entrega ni 1 ni 16, entonces,
            // para esto es que se debe hacer esta pequeña suma y se castea la parte entera.
            int numero = (int)(Math.random() * BOARD_LENGTH) + 1;
            for (int fila=0; fila<BOARD_DIMENSION; fila++) {
                for (int col=0; col<BOARD_DIMENSION; col++) {
                    if (_tokens[fila][col] == 0) { // los arrays int siempre se crean por default con 0
                        _tokens[fila][col] = numero;
                        fila = BOARD_DIMENSION;
                        col = BOARD_DIMENSION;
                        i++;
                    } else if (_tokens[fila][col] == numero) {
                        fila = BOARD_DIMENSION;
                        col = BOARD_DIMENSION;
                    }
                }
            }
        }

        Toast.makeText(this.context, this.context.getString(R.string.msg_startgame), Toast.LENGTH_LONG).show();
    }


    public String[] getCards() {
        return cards;
    }

    public int[][] get_tokens() {
        return _tokens;
    }


    public String getFlipped_card() {
        return flipped_card;
    }

    public void setFlipped_card(String flipped_card) {
        this.flipped_card = flipped_card;
    }

    public String getCurrent_card() {
        return current_card;
    }

    public void setCurrent_card(String current_card) {
        this.current_card = current_card;
    }


}
